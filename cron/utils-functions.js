const rimraf = require('rimraf');
const {
  SHOT_FOLDER_PATH
} = require('../utils/constants');

exports.cleanImages = () => {
  rimraf(`${SHOT_FOLDER_PATH}/*`, () => {
    console.log('Cleaner executed at: ', new Date());
  });
};
