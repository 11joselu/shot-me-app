const cron = require('node-cron');
const {
  cleanImages
} = require('./utils-functions');

if (process.env.CRON_CLENANER_TIME) {
  cron.schedule(process.env.CRON_CLENANER_TIME, function () {
    cleanImages();
  });
}
