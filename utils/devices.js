const deviceDescriptors = (require('puppeteer/DeviceDescriptors'));
const devices = {};
const DESKTOP_NAME = 'Desktop';
const {
  DEFAULT_USER_AGENT
} = require('./constants');

if (!Object.keys(devices).length) {
  for (const device of deviceDescriptors) {
    devices[device.name] = device;
  }
}

const DESKTOP_ATTRIBUTES = {
  'name': DESKTOP_NAME,
  'checked': true,
  'userAgent': DEFAULT_USER_AGENT,
  'viewport': {
    'isMobile': false,
    'hasTouch': false,
  }
};

const DEVICES = Object.assign({}, {
  [DESKTOP_NAME]: DESKTOP_ATTRIBUTES
}, devices);

module.exports = {
  DESKTOP_NAME,
  DESKTOP_ATTRIBUTES,
  DEVICES,
};
