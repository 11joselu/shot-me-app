const {
  body,
} = require('express-validator/check');

function validateQueryArray(type) {
  return body(type)
    .custom(value => {
      if (!value || value.length === 0) {
        throw new Error(`${type} is a required field`);
      }

      if (!Array.isArray(value)) {
        throw new Error(`${type} param must be an array`);
      }

      return true;
    });
}


exports.validateDevices = () => {
  return validateQueryArray('devices');
};

exports.validateUrls = () => {
  return validateQueryArray('url');
};
