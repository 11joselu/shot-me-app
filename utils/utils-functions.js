const {
  DEVICES,
  DESKTOP_NAME
} = require('./devices');

const {
  DEFAULT_USER_AGENT
} = require('./constants');

exports.generateDesktopOptionsByUserInfo = (req) => {
  let {
    width = 1200,
      height = 700,
  } = req.body;

  const desktop = Object.assign({}, DEVICES[DESKTOP_NAME]);
  desktop['userAgent'] = req.get('User-Agent') || DEFAULT_USER_AGENT;

  width = Number(width);
  height = Number(height);

  if (!isNaN(width)) {
    desktop.viewport['width'] = Number(width);
  }

  if (!isNaN(height)) {
    desktop.viewport['height'] = Number(height);
  }

  return desktop;
};
