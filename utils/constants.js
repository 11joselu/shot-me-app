const {
  resolve
} = require('path');

const PUBLIC_FOLDER_PATH = resolve(__dirname, '..', 'public');
const VIEWS_FOLDER_PATH = resolve(__dirname, '..', 'views');
const SHOT_FOLDER_PATH = resolve(PUBLIC_FOLDER_PATH, 'shots');
const THUMBNAIL_MAX_SIZE = 100;
const DEFAULT_USER_AGENT = '"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.117 Mobile Safari/537.36"';

module.exports = {
  PUBLIC_FOLDER_PATH,
  VIEWS_FOLDER_PATH,
  SHOT_FOLDER_PATH,
  THUMBNAIL_MAX_SIZE,
  DEFAULT_USER_AGENT,
};
