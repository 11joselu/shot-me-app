const BlinkDiff = require('blink-diff');

const {
  validationResult,
} = require('express-validator/check');

const {
  DEVICES,
  DESKTOP_NAME,
  DESKTOP_ATTRIBUTES
} = require('../../../utils/devices');

const {
  generateDesktopOptionsByUserInfo,
} = require('../../../utils/utils-functions');

const {
  compareTwoImagesByDevices
} = require('../../../tasks/compareTask');

exports.renderView = (req, res) => {
  res.render('shots/compare', {
    title: 'Comparar imagenes',
    devices: DEVICES,
  });
};

exports.compareImages = async (req, res) => {
  const errors = validationResult(req);
  let {
    url,
    devices
  } = req.body;

  const [
    urlOne,
    urlTwo
  ] = url;

  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.array()
    });
  }

  // Restore desktop attributes on every request.
  DEVICES[DESKTOP_NAME] = DESKTOP_ATTRIBUTES;

  if (devices.includes(DESKTOP_NAME)) {
    DEVICES[DESKTOP_NAME] = generateDesktopOptionsByUserInfo(req);
  }

  devices = devices
    .map((device) => DEVICES[device])
    .filter(d => d);

  const results = await compareTwoImagesByDevices(devices, urlOne, urlTwo);

  return res.json(results);
};

exports.getBlinkCodes = (req, res) => {
  return res.json({
    [BlinkDiff.RESULT_DIFFERENT]: 'RESULT_DIFFERENT',
    [BlinkDiff.RESULT_UNKNOWN]: 'RESULT_UNKNOWN',
    [BlinkDiff.RESULT_SIMILAR]: 'RESULT_SIMILAR',
    [BlinkDiff.RESULT_IDENTICAL]: 'RESULT_IDENTICAL',
  });
};
