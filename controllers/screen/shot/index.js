const {
  validationResult,
} = require('express-validator/check');

const {
  shotTask
} = require('../../../tasks/shotTask');

const {
  generateDesktopOptionsByUserInfo
} = require('../../../utils/utils-functions');

const {
  DEVICES,
  DESKTOP_NAME,
  DESKTOP_ATTRIBUTES
} = require('../../../utils/devices');

exports.render = (req, res) => {
  return res.render('shots/screenShot', {
    title: 'Screen Shot',
    devices: DEVICES
  });
};

exports.shot = async (req, res) => {
  let {
    url,
    devices,
  } = req.body;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.array()
    });
  }

  // Restore desktop attributes on every request.
  DEVICES[DESKTOP_NAME] = DESKTOP_ATTRIBUTES;

  if (devices.includes(DESKTOP_NAME)) {
    DEVICES[DESKTOP_NAME] = generateDesktopOptionsByUserInfo(req);
  }

  const results = await createCaptureByDevices(devices, url);

  return res.json(results);
};

const takeShotByDeviceName = async (url, deviceName) => {
  const device = DEVICES[deviceName];
  const {
    path,
    thumbnail,
    shotID
  } = await shotTask(url, {
    resize: true,
    device,
  });

  const resultDevice = Object.assign({}, device);
  delete resultDevice.checked;

  const result = {
    shotID,
    url: path,
    device: resultDevice,
  };

  if (thumbnail) {
    result.thumbnail = thumbnail;
  }

  return result;
};

const createCaptureByDevices = async (queryDevices, url) => {
  const results = queryDevices
    .filter(d => d in DEVICES)
    .map(async (deviceName) => {
      return takeShotByDeviceName(url, deviceName);
    });

  return await Promise.all(results);
};
