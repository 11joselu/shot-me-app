exports.logout = (req, res) => {
  delete req.session.user;
  return res.redirect('/login');
};
