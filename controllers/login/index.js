const {
  validationResult
} = require('express-validator/check');

exports.renderLogin = (req, res) => {
  res.render('login', {
    title: 'Log in',
  });
};

exports.login = (req, res) => {
  const errors = validationResult(req);
  const {
    email
  } = req.body;

  if (!errors.isEmpty()) {
    return res
      .render('login', {
        errors: errors.array(),
        title: 'Log in',
        email,
      });
  }

  req.session.user = {
    email,
  };

  res.redirect('/');
};
