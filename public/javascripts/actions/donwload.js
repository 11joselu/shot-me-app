import {
  $
} from '../utils/modules';

function donwloadImage(url, name) {
  var a = document.createElement('a');
  a.href = url;
  a.download = name;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

export default function handleDonwloadButton($icons) {
  $icons.off('click');
  $icons.on('click', function () {
    const url = $(this).attr('data-link');
    const name = url.split('/')
      .filter(x => x.indexOf('.png') >= 0)
      .join('');
    donwloadImage(url, name);
  });
}
