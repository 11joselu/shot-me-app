import {
  generateSerializedDataFromForm
} from '../common/utils';
import {
  $
} from '../../utils/modules';
import errorHandler from '../shot/handlers/errorHandler';

import successComparisonHandler from './handlers/successHandler';

let COMPARISON_CODES = {};

$.get('/screen/compare/codes')
  .then(data => {
    COMPARISON_CODES = data;
  });

$('.form--compare').on('submit', function (evt) {
  evt.preventDefault();
  const $loading = $('.loading');
  const $body = $('body');

  $body.addClass('overflow--hidden');
  $loading.removeClass('hidden');

  $.post('/screen/compare', generateSerializedDataFromForm(this))
    .then(data => {
      $loading.addClass('hidden');
      $body.removeClass('overflow--hidden');
      successComparisonHandler(data, COMPARISON_CODES);

      $('html, body')
        .animate({
          scrollTop: $('table').offset().top,
        }, 500, 'swing');
    })
    .catch(e => {
      $loading.addClass('hidden');
      $body.removeClass('overflow--hidden');
      errorHandler(e);
    });
});
