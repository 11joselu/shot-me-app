import {
  $
} from '../../../utils/modules';

function createComparisonTable(data, codes) {
  return (
    `
      <table class="table">
        <thead>
          <tr>
            <th>Device</th>
            <th>URL 1</th>
            <th>URL 2</th>
            <th>Results</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          ${data.map((row) => createResultRow(row, codes)).join('')}
        </tbody>
      </table>
    `
  );
}

function createResultRow(result, codes) {
  return (
    `
      <tr>
        <td>${result.device ? result.device.name : ''}</td>
        <td>${result.urlOne}</td>
        <td>${result.urlTwo}</td>
        <td><span class="status status--${result.code}">${codes[result.code]}</span></td>
        <td><a href="${result.imagePath}">Ver</a></td>
      </tr>
    `
  );
}

function successComparisonHandler(data, codes, parentID = 'comparation-results') {
  $(`#${parentID}`)
    .empty()
    .append(createComparisonTable(data, codes));
}

export default successComparisonHandler;
