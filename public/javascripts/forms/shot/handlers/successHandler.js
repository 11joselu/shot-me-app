import {
  $
} from '../../../utils/modules';
import {
  createShotTable,
  createResultRow
} from '../createShotTable';
import handleDonwloadButton from '../../../actions/donwload';

function successHandler(data = [], parentID = 'results') {
  const table = createShotTable(data);
  const $showWrapper = $(`#${parentID}`);
  if ($showWrapper.children().length) {
    const rows = data.map(createResultRow);
    $showWrapper
      .find('table tbody')
      .append(rows);
  } else {
    $showWrapper
      .removeClass('hidden')
      .append(table);
  }

  handleDonwloadButton($('.image-thumbnail__actions__download'));
}

export default successHandler;
