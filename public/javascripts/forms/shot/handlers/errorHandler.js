import notify from '../../../utils/noty';

function errorHandler({
  responseJSON = {}
}) {
  const {
    errors = []
  } = responseJSON;

  let message = 'Ops! Algo ha ido mal';

  if (errors.length) {
    for (const error of errors) {
      message = error.msg;
      const noty = notify(message, 'error');
      noty.show();
    }
  } else {
    const noty = notify(message, 'error');
    noty.show();
  }
}

export default errorHandler;
