export function createResultRow({
  device,
  url,
  thumbnail
}) {
  return (`
    <tr class="text--center">
      <td>${device.name}</td>
      <td>${device.viewport ? device.viewport.width + 'x' + device.viewport.height : ''}</td>
      <td>
        <a href="${url}">
          <img src="${thumbnail}">
        </a>
      </td>
      <td>
        <div class="text-right image-thumbnail__actions">
          <i data-link="${url}" class="fa fa-download image-thumbnail__actions__download link" title="Descargar" class="download__button"></i>
        </div>
      </td>
    </tr>
  `);
}

export function createShotTable(data) {
  return (
    `
      <table class="table">
        <thead>
          <tr>
            <th>Device</th>
            <th>Viewport</th>
            <th>Thumbnail</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          ${data.map(createResultRow).join('')}
        </tbody>
      </table>
    `
  );
}
