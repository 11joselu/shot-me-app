import {
  $
} from '../../utils/modules';
import {
  generateSerializedDataFromForm
} from '../common/utils';

import successHandler from './handlers/successHandler';
import errorHandler from './handlers/errorHandler';

$('.form-capture:not(.form--compare)').on('submit', function (evt) {
  evt.preventDefault();
  const $loading = $('.loading');
  const $body = $('body');

  $body.addClass('overflow--hidden');
  $loading.removeClass('hidden');

  $.post('/screen/shot', generateSerializedDataFromForm(this))
    .then((data = []) => {
      $loading.addClass('hidden');
      $body.removeClass('overflow--hidden');
      successHandler(data);
      $('html, body')
        .animate({
          scrollTop: $('table').offset().top,
        }, 500, 'swing');
    })
    .catch(e => {
      $loading.addClass('hidden');
      $body.removeClass('overflow--hidden');
      errorHandler(e);
    });
});
