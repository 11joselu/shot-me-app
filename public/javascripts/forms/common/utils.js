import {
  $
} from '../../utils/modules';

export const generateSerializedDataFromForm = function generateSerializedDataFromForm($form) {
  let data = $($form).serialize();
  const DESKTOP_NAME = 'Desktop';

  if (data.indexOf(DESKTOP_NAME) >= 0) {
    const arrayOfQuery = data.split(DESKTOP_NAME);
    const desktopAttribute = `${DESKTOP_NAME}&width=${$(window).width()}&height=${$(window).height()}`;
    data = arrayOfQuery.join(desktopAttribute);
  }

  return data;
};
