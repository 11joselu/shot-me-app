import {
  $
} from '../../utils/modules';
import countCheckedDevices from './count/_devices-count';

countCheckedDevices();

$('.device.device__choice label')
  .on('click', () => {
    setTimeout(countCheckedDevices, 0);
  });
