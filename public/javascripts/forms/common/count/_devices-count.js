import {
  $
} from '../../../utils/modules';

export default function countCheckedDevices() {
  const len = $('.device.device__choice input[type="checkbox"]:checked').length;
  const $warningElement = $('#error-message');
  if (len > 0) {
    $warningElement.addClass('hidden');
    $('button[type="submit"]').removeClass('hidden');
  } else {
    $warningElement.removeClass('hidden');
    $('button[type="submit"]').addClass('hidden');
  }

}
