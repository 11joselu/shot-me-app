import Noty from 'noty';

export default function notify(message, type) {
  return new Noty({
    text: message,
    theme: 'nest',
    type,
    force: true,
    timeout: 4000,
  });
}
