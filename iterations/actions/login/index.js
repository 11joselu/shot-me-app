const LoginPage = require('../../pages/login/LoginPage');

const loginWeb = async (page) => {
  const route = process.env.INITIAL_ROUTE + '/' + LoginPage.url;
  await page.goto(route);

  await page.focus(LoginPage.username);
  await page.keyboard.type(process.env.USER_EMAIL);

  await page.focus(LoginPage.password);
  await page.keyboard.type(process.env.USER_PASSWORD);

  await page.evaluate((LoginPage) => {
    document.querySelector(LoginPage.loginButton).click();
  }, LoginPage);

  await page.waitForNavigation(['networkidle0', 'load', 'domcontentloaded']);
};

const Login = {
  loginWeb,
};

module.exports = Object.freeze(Login);
