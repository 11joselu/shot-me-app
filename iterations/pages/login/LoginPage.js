const url = 'usuario/ingresar';
const username = '#email';
const password = '#password';
const loginButton = '#form_submit';

const LoginPage = {
  url,
  username,
  password,
  loginButton,
};

module.exports = Object.freeze(LoginPage);
