// moment.js is a handy library for displaying dates. We need this in our templates to display things like "Posted 5 minutes ago"
exports.moment = require('moment');

// Dump is a handy debugging function we can use to sort of "console.log" our data
exports.dump = (obj) => JSON.stringify(obj, null, 2);

exports.siteName = 'Habitissimo Image Generator/Comparator';

exports.menu = [{
    slug: '/screen/shot',
    title: 'Capturar pantalla',
    icon: 'fa-camera-retro',
  },
  {
    slug: '/screen/compare',
    title: 'Comparar rutas',
    icon: 'fa-clone',
  },
];
