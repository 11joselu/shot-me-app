const BlinkDiff = require('blink-diff');
const {
  shotTask
} = require('./shotTask');

const {
  promisify
} = require('util');

const {
  PUBLIC_FOLDER_PATH
} = require('../utils/constants');

exports.compareImages = async (imageOnePath, imageTwoPath, device) => {
  const outputPath = imageTwoPath.replace('.png', '-difference.png');

  var diff = new BlinkDiff({
    imageAPath: imageOnePath, // Use file-path
    imageBPath: imageTwoPath,

    thresholdType: BlinkDiff.THRESHOLD_PERCENT,
    threshold: 0.01, // 1% threshold
    imageOutputPath: outputPath,
    verbose: process.env.NODE_ENV === 'production',
  });

  const runPromise = promisify(diff.run);
  const result = await runPromise.call(diff);

  return Object.assign({
    imagePath: outputPath.replace(PUBLIC_FOLDER_PATH, ''),
    device,
  }, result);
}

exports.compareTwoImagesByDevice = async (device, urlOne, urlTwo, opts = {}) => {
  const imageOnePromise = shotTask(urlOne, {
    device,
    ...opts
  });

  const imageTwoPromise = shotTask(urlTwo, {
    device,
    ...opts
  });

  const imageOne = await imageOnePromise;
  const imageTwo = await imageTwoPromise;

  return await this.compareImages(imageOne._path, imageTwo._path, device);
};

exports.compareTwoImagesByDevices = async (devices, urlOne, urlTwo, opts = {}) => {
  const results = [];

  for (const device of devices) {
    const result = await this.compareTwoImagesByDevice(device, urlOne, urlTwo, opts) || {};
    result.urlOne = urlOne;
    result.urlTwo = urlTwo;
    results.push(result);
  }

  return results;
};
