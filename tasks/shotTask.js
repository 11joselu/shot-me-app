const puppeteer = require('puppeteer');
const uuidv4 = require('uuid/v4');
const {
  join
} = require('path');

const sharp = require('sharp');

const {
  SHOT_FOLDER_PATH,
  PUBLIC_FOLDER_PATH,
  THUMBNAIL_MAX_SIZE,
} = require('../utils/constants');

const Login = require('../iterations/actions/login');

async function setOptionsIntoPage(page, device) {
  const {
    name,
    userAgent,
    viewport
  } = device;

  if (name.toLowerCase() !== 'Desktop'.toLocaleLowerCase()) {
    await page.emulate(device);
  } else {
    if (userAgent) {
      await page.setUserAgent(userAgent);
    }
    await page.setViewport(viewport);
  }
}

async function takePageScreenShot(page, path) {
  return await page.screenshot({
    fullPage: true,
    path,
  });
}

exports.resizeImage = async (image, urlToSave) => {
  return await sharp(image)
    .crop(sharp.strategy.entropy)
    .resize(THUMBNAIL_MAX_SIZE, THUMBNAIL_MAX_SIZE)
    .toFile(urlToSave);
};

exports.shotTask = async (url, opts = {}) => {
  const browser = await puppeteer.launch({
    headless: JSON.parse(process.env.HEADLESS),
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });

  try {
    const page = await browser.newPage();
    const shotID = uuidv4();
    const {
      device,
      shouldBeLogged = false,
    } = opts;

    let thumbnailUrl = '';
    let imagePath = `${join(SHOT_FOLDER_PATH, shotID)}.png`;

    if (device) {
      imagePath = imagePath.replace('.png', `-${device.name.split(' ').join('-')}.png`);
      await setOptionsIntoPage(page, device);
    }

    if (shouldBeLogged) {
      await Login.loginWeb(page);
    }

    await page.goto(url);

    const image = await takePageScreenShot(page, imagePath);

    await browser.close();

    if (opts.resize) {
      thumbnailUrl = imagePath.replace('.png', '-thumbnail.png');
      await this.resizeImage(image, thumbnailUrl);
    }

    return {
      shotID: shotID,
      _path: imagePath,
      path: imagePath.replace(PUBLIC_FOLDER_PATH, ''),
      thumbnail: thumbnailUrl.replace(PUBLIC_FOLDER_PATH, '')
    };
  } catch (e) {
    browser.close();
    throw e;
  }
};
