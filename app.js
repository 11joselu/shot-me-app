const express = require('express');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const session = require('express-session');
const compression = require('compression');

const home = require('./routes/home');
const shot = require('./routes/screen/shot');
const compare = require('./routes/screen/compare');
const login = require('./routes/login');
const logout = require('./routes/logout');

const helpers = require('./helpers');
// create our Express app
const app = express();
const errorHandlers = require('./handlers/errorHandlers');

const {
  PUBLIC_FOLDER_PATH,
  VIEWS_FOLDER_PATH
} = require('./utils/constants');
// Cron
require('./cron/cleaner');

// view engine setup
app.set('views', VIEWS_FOLDER_PATH); // this is the folder where we keep our pug files
app.set('view engine', 'pug'); // we use the engine pug, mustache or EJS work great too

// serves up static files from the public folder. Anything in public/ will just be served up as the file it is
app.use(express.static(PUBLIC_FOLDER_PATH));

if (app.get('env') === 'production') {
  app.use(compression());
}
// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// Exposes a bunch of methods for validating data. Used heavily on userController.validateRegister
app.use(expressValidator());
app.use(session({
  secret: process.env.SECRET_KEY,
  resave: false,
  saveUninitialized: true,
}));

// pass variables to our templates + all requests
app.use((req, res, next) => {
  res.locals.h = helpers;
  res.locals.currentPath = req.path;
  res.locals.user = req.session.user;
  next();
});

app.use(function (req, res, next) {
  if (!req.path.includes('login')) {
    if (!req.session.user) {
      return res.redirect('/login');
    }
  }

  return next();
});

app.use('/', home);
app.use('/login', login);
app.use('/logout', logout);
app.use('/screen/shot', shot);
app.use('/screen/compare', compare);

// If that above routes didnt work, we 404 them and forward to error handler
app.use(errorHandlers.notFound);

// Otherwise this was a really bad error we didn't expect! Shoot eh
if (app.get('env') === 'development') {
  /* Development Error Handler - Prints stack trace */
  app.use(errorHandlers.developmentErrors);
} else {
  // production error handler
  app.use(errorHandlers.productionErrors);
}


// done! we export it so we can start the site in start.js
module.exports = app;
