const express = require('express');
const router = express.Router();
const renderController = require('../../controllers/login');
const {
  body,
} = require('express-validator/check');

router.get('/', renderController.renderLogin);
router.post('/', [
  validateCorrectUser('email', validateEmail),
  validateCorrectUser('password', validatePassword)
], renderController.login);

function validateCorrectUser(key, validator) {
  return body(key)
    .custom(value => {
      validator(value);

      return true;
    });
}

function validateEmail(value) {
  if (!value || value !== process.env.USER_EMAIL) {
    throw new Error('Email not found');
  }
}

function validatePassword(value) {
  if (!value || value !== process.env.USER_PASSWORD) {
    throw new Error('Password do not match');
  }
}
module.exports = router;
