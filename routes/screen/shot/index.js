const express = require('express');
const {
  check,
} = require('express-validator/check');

const {
  validateDevices,
} = require('../../../utils/validator/query');

const screenShotController = require('../../../controllers/screen/shot');

const router = express.Router();
const {
  catchErrors
} = require('../../../handlers/errorHandlers');

router.get('/', screenShotController.render);

router.post('/', [
  check('url').isURL(),
  validateDevices(),
], catchErrors(screenShotController.shot));

module.exports = router;
