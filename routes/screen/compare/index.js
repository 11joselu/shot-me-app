const express = require('express');
const router = express.Router();
const compareController = require('../../../controllers/screen/compare');
const {
  validateUrls,
  validateDevices,
} = require('../../../utils/validator/query');

const {
  catchErrors
} = require('../../../handlers/errorHandlers');

router.get('/', compareController.renderView);
router.post('/', [validateUrls(), validateDevices()], catchErrors(compareController.compareImages));
router.get('/codes', compareController.getBlinkCodes);

module.exports = router;
