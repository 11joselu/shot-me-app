const express = require('express');
const router = express.Router();
const renderController = require('../../controllers/logout');

router.get('/', renderController.logout);

module.exports = router;
